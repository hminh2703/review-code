#include <cstdint>
#include <iostream>
#include <vector>

#include <bsoncxx/v_noabi/bsoncxx/view_or_value.hpp>
#include <bsoncxx/v_noabi/bsoncxx/array/view.hpp>
#include <bsoncxx/v_noabi/bsoncxx/array/value.hpp>
#include <bsoncxx/v_noabi/bsoncxx/builder/core.hpp>
#include <bsoncxx/v_noabi/bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>

#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/v_noabi/mongocxx/result/update.hpp>
#include <mongocxx/v_noabi/mongocxx/result/insert_one.hpp>
#include <mongocxx/v_noabi/mongocxx/client.hpp>
#include <mongocxx/v_noabi/mongocxx/database.hpp>
#include <mongocxx/v_noabi/mongocxx/collection.hpp>
#include <mongocxx/v_noabi/mongocxx/instance.hpp>
#include <mongocxx/v_noabi/mongocxx/cursor.hpp>

using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;

void Insert_value(bsoncxx::document::view view_value,mongocxx::database db,std::string collectionName)
{
  mongocxx::collection collection = db[collectionName];
  collection.insert_one(view_value);
}

void Insert_many_value(mongocxx::database db,std::string collectionName)
{
  mongocxx::collection collection = db[collectionName];
  std::vector<bsoncxx::document::value> documents;
    for(int i = 0; i < 5; i++) {
    documents.push_back(
      bsoncxx::builder::stream::document{} << "i" << i << finalize);
    }
    collection.insert_many(documents);
}

void ShowValue(mongocxx::database db,std::string collectionName)
{
  mongocxx::collection collection = db[collectionName];
  mongocxx::cursor cursor = collection.find({});
  std::cout << collectionName << "\n";
  for(auto doc : cursor) {
  std::cout << bsoncxx::to_json(doc) << "\n";
  }
}

void Update_Value(bsoncxx::document::view view_value,mongocxx::database db,std::string collectionName)
{
  std::cout<<"Document Update ....\n";
  mongocxx::collection collection = db[collectionName];
  collection.update_one(document{} << "x" << 500 << finalize,
                      document{} << "$set" << open_document <<
                        "x" << 777 << close_document << finalize);
}

void Delete_Value(bsoncxx::document::view view_value,mongocxx::database db,std::string collectionName)
{
  std::cout<<"Document Delete....\n";
    mongocxx::collection collection = db[collectionName];
    //collection.delete_one(document{} << "x" << 203 << finalize);
    collection.delete_one(view_value);
}

void Delete_Filter(mongocxx::database db,std::string collectionName,int namefilter)
{
  mongocxx::collection collection = db[collectionName];

  bsoncxx::stdx::optional<mongocxx::result::delete_result> result = 
  collection.delete_many(document{} << "i" << open_document << "$gte" << namefilter << close_document << finalize);
    if(result) 
    {
      std::cout << result->deleted_count() << "\n";
    }

}

void Find_value(bsoncxx::document::view view_value,mongocxx::database db,std::string collectionName,std::string findvalue)
{
    mongocxx::collection collection = db[collectionName];
    std::cout <<"Document Find : \n";
    bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result =
    collection.find_one(document{} << "x" << 203 << finalize);
    if(maybe_result) 
    {
      std::cout << bsoncxx::to_json(*maybe_result) << "\n";
    }
}
int main(int, char**) {
    mongocxx::instance instance{}; // This should be done only once.
    mongocxx::client client{mongocxx::uri{}};

//Create Database & Collection
    mongocxx::database db = client["Database"];

    auto builder = bsoncxx::builder::stream::document{};
    bsoncxx::document::value doc_value = builder
    << "name" << "MongoDB"
    << "type" << "database"
    << "count" << 1
    << "versions" << bsoncxx::builder::stream::open_array
    << "v3.2" << "v3.0" << "v2.6"
    << close_array
    << "info" << bsoncxx::builder::stream::open_document
    << "x" << 500
    << "y" << 250
    << bsoncxx::builder::stream::close_document
    << bsoncxx::builder::stream::finalize;

    bsoncxx::document::view view_value = doc_value.view();

    Insert_value(view_value,db,"TestCollection");
    //Insert_many_value(db,"Collection");
    ShowValue(db,"TestCollection");
    Update_Value(view_value,db,"TestCollection");
    ShowValue(db,"TestCollection");
              //Delete Document
    Delete_Value(view_value,db,"TestCollection");
    ShowValue(db,"TestCollection");
              //Delete All Documents That Match a Filter
    //Delete_Filter(db,"Collection",500);
    //Find_value(view_value,db,"Collection","500");
    
/*
 
//Updates    
    collection.update_one(document{} << "i" << 99 << finalize,
                      document{} << "$set" << open_document <<
                        "i" << 110 << close_document << finalize);

//Finds
    mongocxx::cursor cursor = collection.find(
    doc_value.view() << "i" << open_document <<
    "$gt" << 90 <<
    "$lte" << 100
    << close_document << finalize);
*/ 
}